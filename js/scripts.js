//Añadir clase active en nav

$(function () {

	var menues = $(".main-nav__link");

	menues.click(function () {

		menues.removeClass("active");
		$(this).addClass("active");
	});

});



$('.backBtn').click(function () {
	$('.sidebar').removeClass('open');
	$('.overlay-menu').removeClass('open');
});


$('.nav-top__toggle').click(function () {
	$('.sidebar').toggleClass('open');
	$('.overlay-menu').toggleClass('open');
});

$('.overlay-menu').click(function () {
	$('.sidebar').toggleClass('open');
	$('.overlay-menu').toggleClass('open');
});


$(".button-close-menu").click(function () {
	$('.sidebar').removeClass("open");
	$('.overlay-menu').removeClass("open");
});


//Menú fijo al hacer scroll

$(window).scroll(function () {
	var header = $(document).scrollTop();
	var headerHeight = $(".nav-top").outerHeight();

	if (window) {

		if (header > headerHeight) {
			$(".nav-down").addClass("fixed-header");
			$("body").css('margin-top', headerHeight);
		} else {
			$(".nav-down").removeClass("fixed-header");
			$("body").css('margin-top', 0);
		}
		if ($(window).scrollTop() >= 200) {
			$(".nav-down").addClass("in-view");
		} else {
			$(".nav-down").removeClass("in-view");
		}

		$(".nav-down").addClass("in-view");
	} else {
	}

});



tabControl();

/*
We also apply the switch when a viewport change is detected on the fly
(e.g. when you resize the browser window or flip your device from 
portrait mode to landscape). We set a timer with a small delay to run 
it only once when the resizing ends. It's not perfect, but it's better
than have it running constantly during the action of resizing.
*/
var resizeTimer;
$(window).on('resize', function (e) {
	clearTimeout(resizeTimer);
	resizeTimer = setTimeout(function () {
		tabControl();
	}, 250);
});

/*
The function below is responsible for switching the tabs when clicked.
It switches both the tabs and the accordion buttons even if 
only the one or the other can be visible on a screen. We prefer
that in order to have a consistent selection in case the viewport
changes (e.g. when you esize the browser window or flip your 
device from portrait mode to landscape).
*/
function tabControl() {
	var tabs = $('.tabbed-content').find('.tabs');
	if (tabs.is(':visible')) {
		tabs.find('a').on('click', function (event) {
			event.preventDefault();
			var target = $(this).attr('href'),
				tabs = $(this).parents('.tabs'),
				buttons = tabs.find('a'),
				item = tabs.parents('.tabbed-content').find('.item');
			buttons.removeClass('active');
			item.removeClass('active');
			$(this).addClass('active');
			$(target).addClass('active');
		});
	} else {
		$('.item').on('click', function () {
			var container = $(this).parents('.tabbed-content'),
				currId = $(this).attr('id'),
				items = container.find('.item');
			container.find('.tabs a').removeClass('active');
			items.removeClass('active');
			$(this).addClass('active');
			container.find('.tabs a[href$="#' + currId + '"]').addClass('active');
		});
	}
}
